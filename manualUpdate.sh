#!/bin/zsh
echo "Retrieving latest property"
node ~/Projects/node-akamai-orchestrator/cli.js papi-get-property --versions --rules --hostnames --behaviors --criteria --ruleSchema --output ./akamai/cicd.jerola.net.json cicd.jerola.net
echo "TODO: Please make updates to the updated property and press any key to continue."
read
echo "Updating property"
node ~/Projects/node-akamai-orchestrator/cli.js papi-update-property cicd.jerola.net --newVersion --rulesFile ./akamai/cicd.jerola.net.json
echo "Activating property on Staging network."
node ~/Projects/node-akamai-orchestrator/cli.js papi-activate --followProgress --notificationEmail jzorko@akamai.com cicd.jerola.net
echo "TODO: Test property on staging network and press any key to confirm activated staging version."
read
echo "Activating property on production network"
node ~/Projects/node-akamai-orchestrator/cli.js papi-activate --followProgress --notificationEmail jzorko@akamai.com --production --complianceNoProdTraffic --verbose cicd.jerola.net
echo "Retrieving latest property updates"
node ~/Projects/node-akamai-orchestrator/cli.js papi-get-property --versions --rules --hostnames --behaviors --criteria --ruleSchema --output ./akamai/cicd.jerola.net.json cicd.jerola.net
